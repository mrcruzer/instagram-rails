class PicsController < ApplicationController
    before_action :find_pic, only: [:edit, :update, :show, :destroy, :upvote]

    before_action :authenticate_user!, except: [:index, :show]

    def index
        @pics = Pic.all
    end

    def show
    end

    def new
        @pic = Pic.new
    end

    def create
        @pic = current_user.pics.new(pic_params)
        respond_to do |format|
            if @pic.save
              format.html { redirect_to @pic, warning: 'Pic Creada Exitosamente.' }
              format.json { render :show, status: :created, location: @pic }
            else
              format.html { render :new }
              format.json { render json: @pic.errors, status: :unprocessable_entity }
            end
        end
    end

    def edit
    end

     def update
        respond_to do |format|
            if @pic.update(pic_params)
              format.html { redirect_to @pic, warning: 'Pic Editada Exitosamente.' }
              format.json { render :show, status: :ok, location: @pic }
            else
              format.html { render :edit }
              format.json { render json: @pic.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
        @pic.destroy
            respond_to do |format|
                format.html { redirect_to pics_url, warning: 'Pic Borrado Exitosamente.' }
                format.json { head :no_content }
        end
    end

    def upvote
        @pic.upvote_by current_user
        redirect_to @pic
    end

    def find_pic
        @pic = Pic.find(params[:id])
    end

    private
        def pic_params
            params.require(:pic).permit(:title, :description, :image)
        end

    
end
